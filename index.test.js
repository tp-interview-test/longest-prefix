const longestCommonPrefix = require("./index");

test('find "fl" from ["flower","flow","flight"]', () => {
  const strs = ["flower", "flow", "flight"];
  const result = longestCommonPrefix(strs);
  expect(result).toBe("fl");
});

test('find empty text from ["dog","racecar","car"]', () => {
  const strs = ["dog", "racecar", "car"];
  const result = longestCommonPrefix(strs);
  expect(result).toBe("");
});
