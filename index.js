/*

	Write a function to find the longest common prefix string amongst an array of strings.

	If there is no common prefix, return an empty string "".

	Example 1:
	Input: strs = ["flower","flow","flight"]
	Output: "fl"


	Example 2:
	Input: strs = ["dog","racecar","car"]
	Output: ""
	Explanation: There is no common prefix among the input strings.

	Constraints:
	* 1 <= strs.length <= 200
	* 0 <= strs[i].length <= 200
	* strs[i] consists of only lower-case English letters.

*/

function longestCommonPrefix(strs = []) {
  const itemCount = strs.length;

  if (!(itemCount >= 1 && itemCount <= 200)) {
    return "";
  }

  const words = strs[0].split("");

  const longest = [];

  for (const [index, letter] of Object.entries(words)) {
    const result = strs.every((value) => value[index] === letter);

    if (!result) {
      break;
    }

    longest.push(letter);
  }

  return longest.join("");
}

module.exports = longestCommonPrefix;
